**Une fois le projet cloné**

Dans le dossier UserTasks/taches/static : **npm install**

Puis dans le dossier UserTasks : **python manage.py migrate**

Lancer ensuite le serveur : **python manage.py runserver**

Ouvrir dans un navigateur l'adresse : **http://127.0.0.1:8000/**
