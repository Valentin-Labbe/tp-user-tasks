from django.contrib import admin
from taches.models import Task, User

# Register your models here.

class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "formated_creation_date", "colored_due_date")
admin.site.register(Task, TaskAdmin)

class UserAdmin(admin.ModelAdmin):
    list_display = ("username", "email")
admin.site.register(User, UserAdmin)