from django import forms
from django.forms import ModelForm, Textarea
from .models import Task, User
import datetime

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ("username", "email")

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ("name", "due_date", "closed", "description", "owner")
        owner = forms.ModelChoiceField(queryset=User.objects.all().order_by('username'))
        widgets = {
            "due_date": forms.DateInput(attrs={"type": "date", 'value': datetime.date.today().strftime("%Y-%m-%d")}, format=("%Y-%m-%d"))
        }