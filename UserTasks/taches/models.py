from django.db import models
from django.utils.html import format_html
import datetime, locale

class User(models.Model):
    username = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)

    def __str__(self):
        return self.username

class Task(models.Model):
    name = models.CharField(max_length=200)
    creation_date = models.DateField(auto_now_add=True)
    due_date = models.DateField(default=datetime.date.today() + datetime.timedelta(days=7))
    closed = models.BooleanField(default=False)
    description = models.TextField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="tasks", default=None, blank=True, null=True) #blank, null, default

    def colored_due_date(self):
        if self.due_date - datetime.timedelta(days=7) > datetime.date.today():
            color = "green"
        elif self.due_date < datetime.date.today():
            color = "red"
        else:
            color = "orange"
        self.due_date = self.due_date.strftime("%d/%m/%Y")
        return format_html("<span style=color:%s>%s</span>" %(color, self.due_date))

    def formated_creation_date(self):
        return format_html("<span>%s</span>" %(self.creation_date.strftime("%d/%m/%Y")))

    def __str__(self):
        return self.name