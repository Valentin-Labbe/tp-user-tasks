from django.shortcuts import render, redirect, reverse, get_object_or_404
from .models import Task, User
from .forms import UserForm, TaskForm
from django.http import HttpResponse

def home(request):
    return render(request, template_name="base.html")

def tasks_list(request):
    tasks = Task.objects.all().order_by("creation_date")
    return render(request, template_name="tasks-list.html", context={"tasks":tasks})

def users_list(request):
    users = User.objects.all()
    return render(request, template_name="users-list.html", context={"users":users})

def user_detail(request, uid):
    user = User.objects.get(pk=uid)
    tasks = Task.objects.filter(owner=user)
    return render(request, "user-detail.html", context={"user":user, "tasks":tasks})

def task_detail(request, tid):
    task = Task.objects.get(pk=tid)
    return render(request, "task-detail.html", context={"task":task})

def delete_user(request, uid):
    User.objects.get(pk=uid).delete()
    return redirect("users-list")

def delete_task(request, tid):
    Task.objects.get(pk=tid).delete()
    return redirect("tasks-list")

def add_user(request):
    form = UserForm()
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("users-list")
    context = {"form":form}
    return render(request, "add-user.html", context)

def edit_user(request, uid):
    user = User.objects.get(id=uid)
    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect("users-list")
    form = UserForm(instance=user)
    return render(request, "edit-user.html", {"form":form})

def add_task(request):
    form = TaskForm()
    # if len(User.objects.all()) == 0:
    #     return add_user(request)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("tasks-list")
    context = {"form":form}
    return render(request, "add-task.html", context)

def edit_task(request, tid):
    task = Task.objects.get(id=tid)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("tasks-list")
    form = TaskForm(instance=task)
    return render(request, "edit-task.html", {"form":form})