from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("tasks-list", views.tasks_list, name="tasks-list"),
    path("users-list", views.users_list, name="users-list"),
    path("users-list/detail/<uid>", views.user_detail, name="user-detail"),
    path("tasks-list/detail/<tid>", views.task_detail, name="task-detail"),
    path("users-list/detail/<uid>/delete", views.delete_user, name="delete-user"),
    path("tasks-list/detail/<tid>/delete", views.delete_task, name="delete-task"),
    path("users-list/detail/<uid>/edit", views.edit_user, name="edit-user"),
    path("tasks-list/detail/<tid>/edit", views.edit_task, name="edit-task"),
    path("add-user", views.add_user, name="add-user"),
    path("add-task", views.add_task, name="add-task"),
]